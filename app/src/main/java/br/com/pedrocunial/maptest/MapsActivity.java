package br.com.pedrocunial.maptest;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.multidex.MultiDex;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import br.com.pedrocunial.maptest.connect.ConnectAsyncTaskWithoutAlert;
import br.com.pedrocunial.maptest.connect.RetrieveServiceOrderAsync;
import br.com.pedrocunial.maptest.model.MyApplication;
import br.com.pedrocunial.maptest.model.ServiceOrder;
import br.com.pedrocunial.maptest.utils.Converter;
import br.com.pedrocunial.maptest.utils.DrawerItemClickListener;

import static br.com.pedrocunial.maptest.model.PathGoogleMap.makeURL;

public class MapsActivity extends AppCompatActivity
        implements OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener,
        MapsInterface,
        AsyncResponse
{

    private boolean isHamburgerMenuOn = false;

    private int             previewDestinationIndex;
    private int             timePreview;
    private int             currentDestinationIndex;
    private int             index;
    private LatLng          myPosition;
    private String[]        dest;
    private LatLng[]        cesar;
    private String[]        clientName;
    private String[]        osNumber;
    private String[]        serviceDescription;
    private String[]        _id;
    private TextView        destinationView;
    private TextView        problemCodeView;
    private GoogleMap       mMap;
    private ImageButton     mapButton;
    private AlertDialog     dialog;        // Dialog (pop-up)
    private LinearLayout    footerLayout;  // Map footer
    private LocationRequest mLocationRequest;
    private GoogleApiClient mGoogleApiClient;

    private String status;
    private String user;
    // For loading the map
    private ProgressDialog  mProgressDialog = null;

    private LatLngBounds.Builder latLngBuilder; // For map resizing

    // Drawer Navigation
    private String                mActivityTitle;
    private ListView              mDrawerList;
    private DrawerLayout          mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;

    private final int    ZOOM           = 16;
    private final int    LONG_INTERVAL  = 50000;
    private final double LINE_THICKNESS = 1;
    private final String TAG            = "MapApp";
    private final int    SDK            = android.os.Build.VERSION.SDK_INT;

    RetrieveServiceOrderAsync asyncTask = new RetrieveServiceOrderAsync();
    private List<ServiceOrder> serviceOrders;

    private Gson gson;

    private List<String> addresses            = new ArrayList<>();
    private List<String> clients              = new ArrayList<>();
    private List<String> serviceDescriptions  = new ArrayList<>();
    private List<String> osNumbers            = new ArrayList<>();
    private List<String> _ids                 = new ArrayList<>();

    private boolean isDataDownloaded = false;
    private boolean isListPopulated  = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        mProgressDialog = ProgressDialog.show(this, "Conectando...", "Obtendo dados do mapa...",
                true, false);

        // Get service orders from api
        asyncTask.delegate = this;
        asyncTask.execute();

        MyApplication mApp = ((MyApplication)getApplicationContext());
        user = mApp.getGlobalVarValue();

        if (getIntent().getExtras() == null) {
            currentDestinationIndex = 0;
        } else {
            currentDestinationIndex = getIntent().getExtras().getInt("index", 0);
            previewDestinationIndex = getIntent().getExtras().getInt("position", -1);
            status = getIntent().getExtras().getString("status");
        }

        Log.i(TAG, String.valueOf(currentDestinationIndex));

        if (mMap == null) {
            // Obtain the SupportMapFragment and get notified when the map is ready to be used.
            SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                    .findFragmentById(R.id.map);
            mapFragment.getMapAsync(this);
        } else {
            GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);

        }

        Log.i(TAG, "Começando o mapa");

        mActivityTitle = getTitle().toString();

        setupDrawer();

        setupDialog();

        latLngBuilder = new LatLngBounds.Builder();

        buildGoogleApiClient();

    }
    @Override
    public int getIndex()
    {
        return currentDestinationIndex;
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        if(mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setupDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        // Add the buttons
        builder.setPositiveButton("SIM", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User clicked OK button
                if (currentDestinationIndex + 1 < dest.length) {
                    currentDestinationIndex++;
                    markDestination(dest[currentDestinationIndex], "Destino");
                    startActivity(new Intent(MapsActivity.this, StatusActivity.class)
                            .putExtra("index", currentDestinationIndex)
                            .putExtra("_id", _id[currentDestinationIndex]));
                } else {
                    // TODO tratamento de final de ordens de serviço
                }
            }
        });
        builder.setNegativeButton("CANCELAR", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User cancelled the dialog
            }
        });
        builder.setMessage(R.string.arrived);

        // Create the AlertDialog
        dialog = builder.create();
    }

    // Fixing the 64K multidex error
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(newBase);
        MultiDex.install(this);
    }

    public String[] getAddresses() {
        return dest;
    }
    public String[] getClients() { return clientName;}
    public String[] getIds() { return _id;}
    public String[] getOsNumber(){return osNumber;}
    public String[] getOsDescription(){return serviceDescription;}
    public String   getStatus(){return status;}
    public String   getName(){return user;}

    public void setName(String username){
        Bundle extras = getIntent().getExtras();
        if(extras!=null)
            user = extras.getString(username);
    }

    @Override
    protected void onResume()
    {
        super.onResume();
    }

    @Override
    protected void onStop()
    {
        super.onStop();
    }

    private void setupDrawer() {
        // Sets up the drawer menu (hamburger menu)
        String[] options = getResources().getStringArray(R.array.sandwich);
        mDrawerList      = (ListView) findViewById(R.id.left_drawer);
        mDrawerLayout    = (DrawerLayout) findViewById(R.id.drawer_layout);

        // Set the adapter for the list view
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, options);

        View      headerView  = getLayoutInflater().inflate(R.layout.header, null);
        Drawable  picture     = ContextCompat.getDrawable(MapsActivity.this, R.drawable.bob);
        ImageView pictureView = (ImageView) headerView.findViewById(R.id.profile_image);
        TextView  nameView    = (TextView)  headerView.findViewById(R.id.textView1);

        pictureView.setBackground(picture);
        nameView.setText(user);

        assert mDrawerList != null;
        mDrawerList.setAdapter(adapter);
        mDrawerList.addHeaderView(headerView);

        // Makes the menu toggleable
        mDrawerToggle = new MyActionBarDrawerToggle();

        mDrawerLayout.addDrawerListener(mDrawerToggle);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        mDrawerToggle.syncState();

        // Set the list's click listener
        mDrawerList.setOnItemClickListener(new DrawerItemClickListener(this));
    }

    @Override
    protected void onStart() {
        super.onStart();
        // Connect to the client
        mGoogleApiClient.connect();
    }

    private void buildGoogleApiClient() {
        // Starts the google api client (which is an asynchronous task)
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        if (isDataDownloaded) {
            dest               = addresses.toArray(new String[addresses.size()]);
            clientName         = clients.toArray(new String[clients.size()]);
            serviceDescription = serviceDescriptions.toArray(new String[serviceDescriptions.size()]);
            osNumber           = osNumbers.toArray(new String[osNumbers.size()]);
            _id                 = _ids.toArray(new String[_ids.size()]);

            cesar = new LatLng[dest.length];

            isListPopulated = true;

            mProgressDialog.hide();
            defineLayoutsAndViews();
            setDestination();
            startViews();
        }

    }

    private void setDestination()
    {
        // Sets current destination based on the current use of the activity
        if(previewDestinationIndex > -1) {
            index = previewDestinationIndex;
        } else {
            index = currentDestinationIndex;
        }
        markDestination(dest[index], "Destino");
    }

    private void defineLayoutsAndViews() {
        footerLayout          = (LinearLayout) findViewById(R.id.footer);
        destinationView       = (TextView)     findViewById(R.id.dest);
        problemCodeView       = (TextView)     findViewById(R.id.problem_code);
        mapButton             = (ImageButton)  findViewById(R.id.map_button);
    }

    private void startViews()
    {
        assert destinationView            != null; // destination address
        assert mapButton                  != null; // go to maps button
        assert problemCodeView            != null; // problem code
        assert footerLayout               != null; // footer
        destinationView.setText(dest[index]);
        problemCodeView.setText(clientName[index]);

        footerLayout.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                // Define the dialog's properties
                ServiceOrderInformationDialog dialog = ServiceOrderInformationDialog
                        .newInstance(dest[index], clientName[index],
                                     serviceDescription[index], osNumber[index]);
                dialog.show(getSupportFragmentManager(), "Informações");
            }
        });

        // Define mapButton functionality
        mapButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String uri = String.format(Locale.ENGLISH, "geo:%f,%f",
                        cesar[index].latitude,
                        cesar[index].longitude);
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                startActivity(intent);
            }
        });
    }

    private void markDestination(String dest, String title) {
        double[] cesarLatLng = this.getLatLongFromPlace(dest);
        cesar[index] = new LatLng(cesarLatLng[0], cesarLatLng[1]);
        mMap.addMarker(new MarkerOptions().position(cesar[index]).title(title));
        mMap.animateCamera(CameraUpdateFactory.newLatLng(cesar[index]));
        mMap.moveCamera(CameraUpdateFactory.zoomTo(ZOOM));
    }

    public double[] getLatLongFromPlace(String place) {
        double[] latLng = new double[2];

        try {
            Geocoder selectedPlaceGeocoder = new Geocoder(this);
            List<android.location.Address> address;

            address = selectedPlaceGeocoder.getFromLocationName(place, 5);

            if (address != null) {
                android.location.Address location = address.get(0);
                latLng[0] = location.getLatitude();
                latLng[1] = location.getLongitude();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return latLng;
    }

    public void drawPath(String result, int color) {
        try {
            //Tranform the string into a json object
            final JSONObject json              = new JSONObject(result);
            JSONArray        routeArray        = json.getJSONArray("routes");
            JSONObject       routes            = routeArray.getJSONObject(0);
            JSONObject       overviewPolylines = routes.getJSONObject("overview_polyline");

            String       encodedString = overviewPolylines.getString("points");
            List<LatLng> list          = decodePoly(encodedString);

            for (int z = 0; z < list.size() - 1; z++) {
                LatLng src  = list.get(z);
                LatLng dest = list.get(z + 1);
                Polyline line = mMap.addPolyline(new PolylineOptions()
                        .add(new LatLng(src.latitude, src.longitude),
                                new LatLng(dest.latitude, dest.longitude))
                        .width((int) (ZOOM * LINE_THICKNESS))
                        .color(color).geodesic(true));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Context getContext() {
        return MapsActivity.this;
    }

    @Override
    public void setTimePreview(int timePreview) {
        this.timePreview = timePreview;
    }

    @Override
    public int getTimePreview() {
        return this.timePreview;
    }


    private List<LatLng> decodePoly(String encoded) {
        List<LatLng> poly  = new ArrayList<LatLng>();
        int          index = 0, len = encoded.length();
        int          lat   = 0, lng = 0;

        while (index < len) {
            int b, shift = 0, result = 0;

            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);

            int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lat += dlat;

            shift  = 0;
            result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lng += dlng;

            LatLng p = new LatLng((((double) lat / 1E5)),
                    (((double) lng / 1E5)));
            poly.add(p);
        }
        return poly;
    }

    @Override
    public void onLocationChanged(Location location) {
        double lat = location.getLatitude();
        double lng = location.getLongitude();

        //Log.i(TAG, "Starting...");
        if (isDataDownloaded && isListPopulated) {
            updateMyPosition(lat, lng);

            String url = makeURL(myPosition, cesar[index]);
            new ConnectAsyncTaskWithoutAlert(url, this).execute();
            //Log.i(TAG, "Complete!");
        }
    }

    private void updateMyPosition(double lat, double lng) {
        myPosition = new LatLng(lat, lng);

        if(hasArrived(lat, lng)) {
            dialog.show();
            Log.i(TAG, "Chegou");
        } else {
            Log.i(TAG, "Nao chegou");
        }

        mMap.clear();

        Marker destinationMarker = mMap.addMarker(new MarkerOptions()
                .position(cesar[index])
                .title("Destino"));

        Marker locationMarker = mMap.addMarker(new MarkerOptions()
                .position(myPosition).title("You!")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.car)));

        latLngBuilder.include(destinationMarker.getPosition());
        latLngBuilder.include(locationMarker.getPosition());
        LatLngBounds bounds = latLngBuilder.build();
        mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds,
                (int) Converter.pxFromDp(this, 30)));

        if (mMap.getCameraPosition().zoom > ZOOM) {
            mMap.moveCamera(CameraUpdateFactory.zoomTo(ZOOM));
        }

        destinationMarker.showInfoWindow();

    }

    private boolean hasArrived(double lat, double lng) {
        if (isDataDownloaded && isListPopulated) {
            if (((cesar[index].latitude + 0.01 > lat) &&
                    (cesar[index].latitude - 0.01 < lat)) &&
                    ((cesar[index].longitude + 0.01 > lng) &&
                            (cesar[index].longitude - 0.01 < lng))) {
                return true;
            }
            return false;
        } else {
            return false;
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(LONG_INTERVAL); // Update location every 'x' milliseconds

        // Permission check
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest,
                this);
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.i(TAG, "GoogleApiClient connection has been suspend");
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.i(TAG, "GoogleApiClient connection has failed");
    }

    @Override
    public void onBackPressed() {
        // We need to override android's default back button soo that it closes the drawer if
        // it's open instead of quitting the app
        if(isHamburgerMenuOn) {
            mDrawerLayout.closeDrawers();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void processFinish(String output) {
        // populate serviceOrders with service orders from api call
        gson = new Gson();
        serviceOrders  = gson.fromJson(output, new TypeToken<List<ServiceOrder>>(){}.getType());
        for (ServiceOrder serviceOrder : serviceOrders) {
            addresses.add(serviceOrder.getAddress());
            clients.add(serviceOrder.getClient());
            serviceDescriptions.add(serviceOrder.getServiceDescription());
            osNumbers.add(serviceOrder.getNumber());
            _ids.add(serviceOrder.getId());
        }
        isDataDownloaded = true;
        onMapReady(mMap);
    }

    private class MyActionBarDrawerToggle extends ActionBarDrawerToggle {
        public MyActionBarDrawerToggle() {
            super(MapsActivity.this, MapsActivity.this.mDrawerLayout, R.string.drawer_open,
                    R.string.drawer_close);
        }

        /** Called when a drawer has settled in a completely open state. */
        public void onDrawerOpened(View drawerView) {
            super.onDrawerOpened(drawerView);
            getSupportActionBar().setTitle("Opções");
            isHamburgerMenuOn = true;
            invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
        }

        /** Called when a drawer has settled in a completely closed state. */
        public void onDrawerClosed(View view) {
            super.onDrawerClosed(view);
            getSupportActionBar().setTitle(mActivityTitle);
            isHamburgerMenuOn = false;
            invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
        }
    }
}
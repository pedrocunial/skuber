package br.com.pedrocunial.maptest;

/**
 * Created by summerjob on 27/07/16.
 */
public interface AsyncResponse {
    void processFinish(String output);
}

package br.com.pedrocunial.maptest.utils;

import android.util.Log;

import java.io.IOException;

/**
 * Created by summerjob on 26/07/16.
 */
public class InvalidOptionException extends Exception
{

    // Custom exception created for dealing with an invalid status set when dealing with an OS

    public InvalidOptionException()
    {
        super();
        Log.i("InvalidOptionException", "The option chosen is invalid");
    }

}

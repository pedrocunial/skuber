package br.com.pedrocunial.maptest;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

import br.com.pedrocunial.maptest.utils.InvalidOptionException;
import br.com.pedrocunial.maptest.model.ServiceOrder;
import br.com.pedrocunial.maptest.utils.OSArrayAdapter;

public class ScheduleActivity extends AppCompatActivity {

    private ListView                mListView;
    private OSArrayAdapter          mArrayAdapter;
    private ArrayList<ServiceOrder> serviceOrderList;

    private final String TAG ="ScheduleActivity";

    private void getListView()
    {
        if(mListView == null) {
            mListView = (ListView) findViewById(R.id.os_list_view);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_schedule);
        getListView();

        // Get addresses (or value that we'll use) and set them in the listview
        int      index          = 0;
        Bundle   extras         = getIntent().getExtras();
        String[] addresses      = null;
        String[] clients        = null;
        String[] osDescriptions = null;
        String[] osNumbers      = null;
        String   status         = null;
        if(extras != null) {
            addresses        = extras.getStringArray("addresses");
            index            = extras.getInt("index");
            clients          = extras.getStringArray("clients");
            osDescriptions   = extras.getStringArray("osDescription");
            osNumbers        = extras.getStringArray("osNumber");
            status           = extras.getString("status");
        } else {
            Log.d(TAG, "Could not get bundle");
        }
        serviceOrderList = new ArrayList<>();
        assert addresses != null;

        // The index will tell us which Service Orders have already been done, soo we can
        // iterate through our objects setting their statuses based on that
        if(index == 0) {
            for(int i = 0; i < addresses.length; i++) {
                    try {
                        serviceOrderList.add(new ServiceOrder(
                                this, addresses[i], clients[i],getResources().getString(R.string.pendente), osNumbers[i], osDescriptions[i]));
                    } catch(InvalidOptionException e) {
                        e.printStackTrace();
                    }
            }
        } else {
            for(int i = 0; i < addresses.length; i++) {
                if(i < index) {
                    try {
                        serviceOrderList.add(new ServiceOrder(this, addresses[i],clients[i],
                                status, osNumbers[i], osDescriptions[i]));
                    } catch(InvalidOptionException e) {
                        e.printStackTrace();
                    }
                } else {
                    try {
                        serviceOrderList.add(new ServiceOrder(
                                this, addresses[i], clients[i], getResources().getString(R.string.pendente), osNumbers[i], osDescriptions[i]));
                    } catch(InvalidOptionException e) {
                        e.printStackTrace();
                    }
                }
            }
        }

        mArrayAdapter = new OSArrayAdapter(this, 0, serviceOrderList);

        // Set adapter for the list
        mListView.setAdapter(mArrayAdapter);
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                // Get clicked item
                Log.d(TAG, "Clicked on List View");
                ServiceOrder selectedOS = mArrayAdapter.getItem(position);
                String selectedAddress = selectedOS.getAddress();
                // Example
                Toast.makeText(ScheduleActivity.this, "You choose " + selectedAddress,
                               Toast.LENGTH_SHORT).show();
            }
        });
    }
}

package br.com.pedrocunial.maptest;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import org.w3c.dom.Text;

/**
 * Created by summerjob on 27/07/16.
 */
public class ServiceOrderInformationDialog extends BaseDialogFragment<ServiceOrderInformationDialog>
{

    public static ServiceOrderInformationDialog newInstance(String address, String clientName,
                                                           String serviceType, String serviceCode)
    {
        // This is what we should use to create new dialogs, it'll let us set the values for
        // the text fields (TextView) in our dialog
        ServiceOrderInformationDialog frag = new ServiceOrderInformationDialog();
        Bundle args = new Bundle();
        args.putString("address", address);
        args.putString("clientName", clientName);
        args.putString("serviceType", serviceType);
        args.putString("serviceCode", serviceCode);
        frag.setArguments(args);
        return frag;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        // Getting the Layout Inflater
        LayoutInflater inflater = getActivity().getLayoutInflater();

        // Get our view
        View view = inflater.inflate(R.layout.dialog_service_order_information, null);

        // Inflate the layout and set its design to the one we made
        // Pass null as the parent view because it's going in the dialog layout
        builder.setView(view);

        // Set text values
        String address = getArguments().getString("address", null);
        String clientName = getArguments().getString("clientName", null);
        String serviceType = getArguments().getString("serviceType", null);
        String serviceCode = getArguments().getString("serviceCode", null);

        // Defining the Text View fields
        TextView addressTextView = (TextView)
                view.findViewById(R.id.advanced_address_dialog);
        TextView clientNameTextView = (TextView)
                view.findViewById(R.id.advanced_client_name_dialog);
        TextView serviceTypeTextView = (TextView)
                view.findViewById(R.id.advanced_service_type_dialog);
        TextView serviceCodeTextView = (TextView)
                view.findViewById(R.id.advanced_service_code_dialog);

        // Set text view values
        addressTextView.setText(address);
        clientNameTextView.setText(clientName);
        serviceTypeTextView.setText(serviceType);
        serviceCodeTextView.setText(serviceCode);

        // return the created dialog
        return builder.create();
    }
}

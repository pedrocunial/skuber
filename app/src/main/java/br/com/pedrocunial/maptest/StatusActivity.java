package br.com.pedrocunial.maptest;

import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.jaredrummler.materialspinner.MaterialSpinner;
import com.rengwuxian.materialedittext.MaterialEditText;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class StatusActivity extends AppCompatActivity {

    //View Parameters
    private ImageView            image;
    private FloatingActionButton buttonNext;
    private FloatingActionButton imageButton;
    private MaterialEditText     commentText;

    //Email Parameters
    private Bitmap  thumbnail;
    private File    Picture;
    private File    folder;
    private boolean foto = false;

    private int index;

    private final int    REQUEST_IMAGE_CAPTURE = 1;
    private final int    EMAIL_SEND_SUCCESS    = 2;
    private final int    EMAIL_SEND_FAIL       = 3;
    private final String TAG                   = "StatusActivity";
    private final String EMAIL_SUBJECT         = "Assistência técnica ZeusTV";

    private boolean mailClientOpened = false;
    private String  status           = null;
    private String _id            = null;

    // Knowing if the user left the activity to enter an email client
    @Override
    protected void onResume() {
        super.onResume();
        mailClientOpened = false;
    }

    @Override
    protected void onStop() {
        super.onStop();
        mailClientOpened = true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_status);
        if(getIntent().getExtras() == null) {
            index = 0;
        } else {
            index = getIntent().getExtras().getInt("index");
            _id   = getIntent().getExtras().getString("_id");

        }
        //Get View Components
        buttonNext  = (FloatingActionButton) findViewById(R.id.next_btn);
        imageButton = (FloatingActionButton) findViewById(R.id.camera_btn);
        commentText = (MaterialEditText) findViewById(R.id.comment_window);
        image       = (ImageView) findViewById(R.id.image_comment);

        //sets spinner list
        setSpinnerList();

        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                takePicture(view);
            }
        });
        buttonNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                // Create the listener for our Yes / No buttons
                DialogInterface.OnClickListener dialogOnClickListener =
                        generateDialogOnClickListener();
                // Create the dialog for ensuring the user actually wants to close the service order
                AlertDialog.Builder builder = new AlertDialog.Builder(StatusActivity.this);
                builder.setMessage("Deseja finalizar a Ordem de Serviço?")
                        .setPositiveButton("Sim", dialogOnClickListener)
                        .setNegativeButton("Não", dialogOnClickListener)
                        .show();
            }
        });
    }

    private DialogInterface.OnClickListener generateDialogOnClickListener()
    {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int position)
            {
                switch(position)
                {
                    case DialogInterface.BUTTON_POSITIVE:
                        // Yes button clicked
                        final String comment  = commentText.getText().toString();
                        final String file_url = _id;
                        final String[] userData = {status, comment, file_url};
                        sendEmail(userData);
                        break;
                    case DialogInterface.BUTTON_NEGATIVE:
                        // No button clicked
                        break;
                }
            }
        };
        return dialogClickListener;
    }

    private void takePicture(View view) {
        // Opens camera app to take a picture
        // First, we need to toggle this boolean to know if we should or not append a picture
        // to our e-mail
        foto = true;
        // Create the File where the photo should go
        folder = new File(Environment.getExternalStorageDirectory().toString() + "/ZeusTV/");
        folder.mkdirs();
        // Creates the intent
        Intent takePictureIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        // Sets the path for the picture
        Picture = new File(folder.toString(), "problemPicture.jpg");
        // Save the path
        Uri uriSavedImage = Uri.fromFile(Picture);
        // Add it to the intent
        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, uriSavedImage);
        // Start Activity
        startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
    }

    private void deleteFilesCLI(String path) {
        // Deletes all files from a directory using the command line
        File file = new File(path);
        if(file.exists()) {
            String deleteCommand = "rm -r" + path;
            Runtime runtime = Runtime.getRuntime();
            try {
                runtime.exec(deleteCommand);
            } catch(RuntimeException e) {
                Log.i(TAG, "Could not find nor delete contents of directory");
            } catch(IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void endActivity()
    {
        // Back to Maps Activity
        // Delete the picture
        if(foto) {
            deleteFilesCLI(folder.toString());
        }

        Toast.makeText(getApplicationContext(), "OS Finalizada",Toast.LENGTH_SHORT).show();
        Intent it = new Intent(StatusActivity.this, MapsActivity.class)
                .putExtra("status", status)
                .putExtra("index", index);

        startActivity(it);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        //Converts picture to PNG
        Log.d(TAG, "inside activityResult");

        if ((requestCode == REQUEST_IMAGE_CAPTURE) && (resultCode == RESULT_OK)) {
            thumbnail = BitmapFactory.decodeFile(Picture.getPath());
            image.setImageBitmap(thumbnail);
        } else if(requestCode == EMAIL_SEND_SUCCESS && mailClientOpened) {
            endActivity();
        } else if(requestCode == EMAIL_SEND_FAIL && mailClientOpened) {
            endActivity();
        }
    }

    public void setSpinnerList()
    {
        //Set Spinner Dropdown List
        MaterialSpinner spinner = (MaterialSpinner) findViewById(R.id.status_op);

        // Items
        final List<String> categories = new ArrayList<String>();
        categories.add("Pendente");
        categories.add("Resolvido");
        categories.add("Precisa Retornar");
        categories.add("OS Errada");
        categories.add("Não Resolvido");

        // Setting them in the spinner
        spinner.setItems(categories);

        // Adding the listener to the spinner
        spinner.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener<String>() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long l, String item) {
                // Selects dropdown item
                switch (position){
                    case 0:
                        status = categories.get(position);
                        enableViewComponents(false);
                        break;
                    case 1:
                        status = categories.get(position);
                        enableViewComponents(true);
                        break;
                    case 2:
                        status = categories.get(position);
                        enableViewComponents(true);
                        break;
                    case 3:
                        status = categories.get(position);
                        enableViewComponents(true);
                        break;
                    case 4:
                        status = categories.get(position);
                        enableViewComponents(true);
                        break;
                }
            }
        });
    }

    public void enableViewComponents(boolean b){
        //Enables Send button and Comment Edit Text to user
        commentText.setEnabled(b);
        buttonNext.setEnabled(b);
    }

    private void sendEmail(String[] userData)
    {
        // First, we're going to send it to our DB, then we do all of that email stuff
        UpdateOS updateOSTask = new UpdateOS(new UpdateOS.updateOsResponse()
        {
            @Override
            public void processFinish(Object output)
            {
                if(output.equals("200")) {
                    endActivity();
                } else if(output.equals("404")) {
                    Toast.makeText(getApplicationContext(), "OS não encontrada", Toast.LENGTH_LONG)
                         .show();
                } else {
                    Toast.makeText(getApplicationContext(), "Servidor indisponível",
                                   Toast.LENGTH_SHORT).show();
                }
            }
        });
        updateOSTask.execute(userData);

        // Creates an email with the infos from this page and give the user
        // the option to send it
     /*   Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("message/rcf822");
        intent.putExtra(Intent.EXTRA_EMAIL, new String[] {"pcc@cesar.org.br"});
        intent.putExtra(Intent.EXTRA_SUBJECT, EMAIL_SUBJECT);
        intent.putExtra(Intent.EXTRA_TEXT, commentText.getText());
        if(foto) {
            intent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(Picture));
            try {
                startActivityForResult(intent, EMAIL_SEND_SUCCESS);
            } catch(ActivityNotFoundException e) {
                Log.i(TAG, "Communication failed");
            }
        } else {
            try {
                startActivityForResult(intent, EMAIL_SEND_FAIL);
            } catch(ActivityNotFoundException e) {
                Log.i(TAG, "Communication failed");
            }
        }*/
    }
}

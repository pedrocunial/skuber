package br.com.pedrocunial.maptest.connect;

import android.os.AsyncTask;
import android.util.Log;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import br.com.pedrocunial.maptest.AsyncResponse;

/**
 * Created by summerjob on 26/07/16.
 */
public class RetrieveServiceOrderAsync extends AsyncTask<Void, Void, String> {

    public AsyncResponse delegate = null;

    @Override
    protected String doInBackground(Void... voids) {
        try {
            HttpURLConnection conn = Connection.create("serviceorders");
            try {
                BufferedReader bufferedReader =
                        new BufferedReader(new InputStreamReader(conn.getInputStream()));
                StringBuilder stringBuilder = new StringBuilder();
                String        line;
                while((line = bufferedReader.readLine()) != null) {
                    stringBuilder.append(line).append("\n");
                }
                bufferedReader.close();
                return stringBuilder.toString();
            } finally {
                conn.disconnect();
            }
        } catch (NullPointerException e) {
            // Asserting the connection actually happened in a more specific way
            Log.e("ERROR", "Connection value is null");
        } catch (Exception e){
            // Asserting for any other possible error
            Log.e("ERROR", e.getMessage(), e);
        }
        return null;
    }

    protected void onPostExecute(String response) {

        if (response == null) {
            response = "THERE WAS AN ERROR";
            Log.i("ERROR", response);
        } else {
            delegate.processFinish(response);
        }

    }

}

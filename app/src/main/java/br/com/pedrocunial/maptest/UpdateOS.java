package br.com.pedrocunial.maptest;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

import br.com.pedrocunial.maptest.connect.Connection;

/**
 * Created by summerjob on 21/07/16.
 */
//
// Asynchronous class to request user login using API
public class UpdateOS extends AsyncTask<String, String, String>
{
    BufferedReader    reader = null;
    HttpURLConnection con    = null;
    String            status = "";

    @Override
    protected String doInBackground(String... userData)
    {
        startPostConnection(userData[0], userData[1], userData[2]);
        status = checkConnection(con);
        return status;
    }

    private void startPostConnection(String status, String comment, String post_url)
    {
        con = Connection.create("serviceorders/" + post_url);
        try {
           //uri = new URL(post_url);
           // con = (HttpURLConnection) uri.openConnection();
            con.setRequestMethod("PUT"); //type: POST, PUT, DELETE, GET
            con.setDoOutput(true);
            con.setDoInput(true);
            con.setConnectTimeout(60000); //60 secs
            con.setReadTimeout(60000); //60 secs
            con.setRequestProperty("Accept-Encoding", "application/json");
            con.setRequestProperty("Content-Type", "application/json");
            sendJsonPost(status, comment);
        } catch(ProtocolException e) {
            e.printStackTrace();
        } catch(IOException e) {
            e.printStackTrace();
        }
    }

    //Sends JSON params
    private void sendJsonPost(String status, String comment)
    {
        JSONObject jsonParam = new JSONObject();
        try {
            jsonParam.put("status", status);
            jsonParam.put("comment", comment);
            OutputStreamWriter out = new OutputStreamWriter(con.getOutputStream());
            out.write(jsonParam.toString());
            out.close();
        } catch(JSONException e) {
            e.printStackTrace();
        } catch(IOException e) {
            e.printStackTrace();
        }
    }

    //Returns connection response code
    private String checkConnection(HttpURLConnection urlConnection)
    {
        int    HttpResult = 0;
        String status     = "";
        try {
            HttpResult = urlConnection.getResponseCode();
            //if username and password exists on database, then return 200 status
            if(HttpResult == HttpURLConnection.HTTP_OK)
                return status= "200";
            //if username or password doesn't exists on database, then return 404 status
            if(HttpResult == HttpURLConnection.HTTP_NOT_FOUND)
                return status = "404";
            //if server not available
            if(HttpResult == HttpURLConnection.HTTP_INTERNAL_ERROR)
                return status = "500";

        } catch(IOException e) {
            e.printStackTrace();
        }
        return status;
    }

    @Override
    protected void onPostExecute(String result)
    {
        Log.i("Output", result);
        delegate.processFinish(result);
    }

    //Update OS Interface
    public interface updateOsResponse
    {
        void processFinish(Object output);
    }

    //Callback Interface
    public updateOsResponse delegate = null;

    //LoginRequestActivity constructor
    public UpdateOS(updateOsResponse osResponse)
    {
        delegate = osResponse;
    }
}
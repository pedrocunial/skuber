package br.com.pedrocunial.maptest.model;

import android.app.Service;
import android.content.Context;
import android.content.res.Resources;

import java.util.Date;

import br.com.pedrocunial.maptest.R;
import br.com.pedrocunial.maptest.utils.InvalidOptionException;

/**
 * Created by summerjob on 26/07/16.
 */
public class ServiceOrder
{
    private String number;
    private String createdAt;
    private String scheduledTo;
    private String dayPeriod;
    private String status;
    private String tvPackage;
    private String serviceType;
    private String serviceDescription;
    private String client;
    private String address;
    private String comment;
    private String _id;


    // Generic constructor
    public ServiceOrder(Context context, String address) {
        this.address = address;
    }

    public ServiceOrder(Context context, String address, String createdAt, String scheduledTo,
                        String dayPeriod, String status, String tvPackage, String serviceType,
                        String serviceDescription, String comment, String _id)
    {
        this.createdAt          = createdAt;
        this.scheduledTo        = scheduledTo;
        this.dayPeriod          = dayPeriod;
        this.status             = status;
        this.tvPackage          = tvPackage;
        this.serviceType        = serviceType;
        this.serviceDescription = serviceDescription;
        this.comment            = comment;
        this.address            = address;
        this._id                = _id;
        this.status             = context.getResources().getString(R.string.pendente);
    }

    // Constructor for when receiving basic information (temp?)
    public ServiceOrder(Context context, String address, String client, String status, String osNumber, String osDescription)
            throws InvalidOptionException
    {
        Resources cResources = context.getResources();
        if(status.equals(cResources.getString(R.string.pendente)) ||
           status.equals(cResources.getString(R.string.resolvido))||
           status.equals(cResources.getString(R.string.nao_resolvido))||
           status.equals(cResources.getString(R.string.os_errada))||
                status.equals(cResources.getString(R.string.retorno))) {
            this.address       = address;
            this.status        = status;
            this.client        = client;
            this.number      = osNumber;
            this.serviceDescription = osDescription;
        } else {
            throw new InvalidOptionException();
        }
    }

    public String getAddress()
    {
        return address;
    }

    public String getStatus()
    {
        return status;
    }

    public void setStatus(String status)
    {
        this.status = status;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getScheduledTo() {
        return scheduledTo;
    }

    public void setScheduledTo(String scheduledTo) {
        this.scheduledTo = scheduledTo;
    }

    public String getDayPeriod() {
        return dayPeriod;
    }

    public void setDayPeriod(String dayPeriod) {
        this.dayPeriod = dayPeriod;
    }

    public String getTvPackage() {
        return tvPackage;
    }

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    public String getServiceDescription() {
        return serviceDescription;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public void setServiceDescription(String serviceDescription) {
        this.serviceDescription = serviceDescription;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }

    public String getId() { return _id;}

    public void setId(String _id) {
        this._id = _id;
    }
}

package br.com.pedrocunial.maptest.utils;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Objects;

import br.com.pedrocunial.maptest.MapsActivity;
import br.com.pedrocunial.maptest.R;
import br.com.pedrocunial.maptest.ServiceOrderInformationDialog;
import br.com.pedrocunial.maptest.model.ServiceOrder;

/**
 * Created by summerjob on 26/07/16.
 */
public class OSArrayAdapter extends ArrayAdapter<ServiceOrder>
{
    private Context                 context;
    private TextView                statusTextView;
    private ImageView               directionsButton;
    private ArrayList<String>       statuses;
    private ArrayList<String>       addresses;
    private ArrayList<String>       clients;
    private ArrayList<String>       osNumbers;
    private ArrayList<String>       osDescriptions;
    private ArrayList<ServiceOrder> infos;

    public OSArrayAdapter(Context context, int resource, ArrayList<ServiceOrder> infos)
    {
        // Constructor
        super(context, resource, infos);
        this.context        = context;
        this.addresses      = new ArrayList<>();
        this.statuses       = new ArrayList<>();
        this.clients        = new ArrayList<>();
        this.osDescriptions = new ArrayList<>();
        this.osNumbers      = new ArrayList<>();
        this.infos          = infos;
        // set values for the objects
        for(ServiceOrder info : this.infos) {
            this.addresses.add(info.getAddress());
            this.statuses.add(info.getStatus());
            this.clients.add(info.getClient());
            this.osNumbers.add(info.getNumber());
            this.osDescriptions.add(info.getServiceDescription());
        }
    }

    public View getView(final int position, View currentView, ViewGroup parent)
    {
        // Called when rendering the list
        // Get property we're displaying
        String address       = addresses.get(position);
        String status        = statuses.get(position);
        String client        = clients.get(position);
        String osNumber      = osNumbers.get(position);
        String osDescription = osDescriptions.get(position);

        // Get the inflater and inflate the XML for it
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(
                Activity.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.os_list_view, null);

        TextView addressTextView = (TextView)  view.findViewById(R.id.address_text_view);
        statusTextView           = (TextView)  view.findViewById(R.id.status_text_view);
        directionsButton         = (ImageView) view.findViewById(R.id.image_button_goto_map);

        // Setting address in the text view
        // Display "..." trimming the address if it's too long
        if(address.length() > 34) {
            address = address.substring(0, 30) + "...";
        }
        addressTextView.setText(address);

        // Setting the status in the text view
        try {
            setStatus(status);
        } catch(InvalidOptionException e) {
            e.printStackTrace();
        }

        // Create and set the listener for the layout itself
        view = createLayoutAndSetListener(view, address, client,
                                          osDescription, osNumber);

        directionsButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(context, MapsActivity.class);
                intent.putExtra("position", position);
                context.startActivity(intent);
            }
        });

        // Finally, we return it!
        return view;
    }

    private View createLayoutAndSetListener(View view, final String address, final String clientName,
                                            final String serviceType, final String serviceCode)
    {
        view.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                // Define the dialog's properties
                ServiceOrderInformationDialog dialog = ServiceOrderInformationDialog
                        .newInstance(address, clientName, serviceType, serviceCode);
                dialog.show(((AppCompatActivity) context)
                                    .getSupportFragmentManager(), "Informações");
            }
        });
        return view;
    }

    @Override
    public ServiceOrder getItem(int position)
    {
        return infos.get(position);
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    public void setStatus(String status) throws InvalidOptionException
    {
        Resources cResources = context.getResources();
        if(Objects.equals(status, cResources.getString(R.string.resolvido))||
                Objects.equals(status, cResources.getString(R.string.os_errada))||
                Objects.equals(status, cResources.getString(R.string.nao_resolvido))||
        Objects.equals(status, cResources.getString(R.string.retorno))) {
            statusTextView.setText(status);
            statusTextView.setTextColor(cResources.getColor(R.color.doneGreen));
        } else if(Objects.equals(status, cResources.getString(R.string.pendente))) {
            statusTextView.setText(status);
            statusTextView.setTextColor(cResources.getColor(R.color.colorVivid));
        } else {
            throw new InvalidOptionException();
        }
    }
}

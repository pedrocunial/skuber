package br.com.pedrocunial.maptest.connect;

import android.util.Log;

import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by summerjob on 01/08/16.
 */
public final class Connection {

    private Connection(){}

    public static HttpURLConnection create(String route){
        HttpURLConnection urlConnection = null;
        try{
            URL url = new URL("http://zeustv-summerjob.rhcloud.com/" + route);
            urlConnection = (HttpURLConnection) url.openConnection();
        } catch (Exception e){
            Log.e("ERROR", e.getMessage(), e);
        }
        return urlConnection;
    }

    public static void disconnect(HttpURLConnection connection){
        connection.disconnect();
    }

}

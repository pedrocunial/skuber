package br.com.pedrocunial.maptest;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import br.com.pedrocunial.maptest.connect.Connection;

/**
 * Created by summerjob on 21/07/16.
 */
//
// Asynchronous class to request user login using API
public class LoginRequest extends AsyncTask<String, String, String> {
    BufferedReader reader = null;
    HttpURLConnection conn = null;
    HttpURLConnection getconn = null;
    String status="200";
    final Context mcontext = null;
    @Override
    protected String doInBackground(String... userData) {
        startPostConnection(userData[0],userData[1],userData[2]);
        startGetConnection(userData[0]);
        return status;
    }

    private void startGetConnection(String username) {
        try {
            getconn = Connection.create("users/"+username);
            try{
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(getconn.getInputStream()));
                StringBuilder stringBuilder   = new StringBuilder();
                String line;
                while((line = bufferedReader.readLine()) != null){
                    stringBuilder.append(line).append("\n");
                }
                bufferedReader.close();
                String firstname = null,lastname = null;
                String t = "200";
                JSONObject job = new JSONObject(stringBuilder.toString());
                firstname = job.getString("firstname");
                lastname =  job.getString("lastname");
                status = status.concat(" " + firstname + " " + lastname);
            } finally {
                getconn.disconnect();
            }
        } catch (Exception e){
            Log.e("ERROR", e.getMessage(), e);
        }
    }

    private void startPostConnection(String username, String password, String post_url){
        try {
            conn = Connection.create("login");
            //Start API connection
          //  URL url = new URL(post_url);
          //  urlConnection =  (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setRequestMethod("POST");  //Start post request
            conn.setRequestProperty("Content-Type","application/json");
            conn.connect();
           // startGetConnection(username);
            sendJsonPost(username, password);//Send json to server
            status = checkConnection(conn); //Get server response code and return status to LoginActivity
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            //Nothing left to do, close connection and reader
            if(conn!=null){
                conn.disconnect();
            }
            if(reader!=null){
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
    //Sends JSON params
    private void sendJsonPost(String username, String password){
        JSONObject jsonParam = new JSONObject();
        try {
            jsonParam.put("username", username);
            jsonParam.put("password", password);
            OutputStreamWriter out = new OutputStreamWriter(conn.getOutputStream());
            out.write(jsonParam.toString());
            out.close();
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    //Returns connection response code
    private String checkConnection(HttpURLConnection urlConnection){
        int HttpResult = 0;
        String status="";
        try {
            HttpResult = urlConnection.getResponseCode();
            //if username and password exists on database, then return 200 status
            if(HttpResult == HttpURLConnection.HTTP_OK)
                return status = "200";
            //if username or password doesn't exists on database, then return 404 status
            if(HttpResult == HttpURLConnection.HTTP_NOT_FOUND)
                return status = "404";
            //if server not available
            if(HttpResult == HttpURLConnection.HTTP_INTERNAL_ERROR)
                return status="500";

        } catch (IOException e) {
            e.printStackTrace();
        }
        return status;
    }
    @Override
    protected void onPostExecute(String result) {
        Log.i("Output", result);
        delegate.processFinish(result);
    }
    //Login Interface
    public interface LoginResponse {
        void processFinish(Object output);
    }
    //Callback Interface
    public LoginResponse delegate = null;
    //LoginRequestActivity constructor
    public LoginRequest(LoginResponse loginResponse) {
        delegate = loginResponse;
    }

}
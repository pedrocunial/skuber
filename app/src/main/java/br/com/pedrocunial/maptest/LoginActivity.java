package br.com.pedrocunial.maptest;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.rengwuxian.materialedittext.MaterialEditText;

import br.com.pedrocunial.maptest.model.MyApplication;

/**
 * Created by summerjob on 07/07/16.
 */
public class LoginActivity extends AppCompatActivity {
    // Atributes
    private EditText username;
    private EditText password;
    private Button   enterButton;

    private CollapsingToolbarLayout collapsingToolbarLayout;

    private final String TAG = "LoginActivity";

    private static final String EXTRA_IMAGE = "br.com.pedrocunial.maptest.extraImage";
    private static final String EXTRA_TITLE = "br.com.pedrocunial.maptest.extraTitle";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.requestWindowFeature(getWindow().FEATURE_NO_TITLE);
        setContentView(R.layout.login);

        assembleLayout();

        Button enterButton = (Button) findViewById(R.id.enter_btn);
        enterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Login based on our DB values
                Log.d(TAG, "Starting login process");
                final String fileUrl   = "login";
                final String user      = username.getText().toString();
                final String pass      = password.getText().toString();
                final String[] userData = {user, pass, fileUrl};
                httpRequest(userData);
            }
        });
    }

    private void assembleLayout()
    {
        ViewCompat.setTransitionName(findViewById(R.id.app_bar_layout), EXTRA_IMAGE);
        String itemTitle = getIntent().getStringExtra(EXTRA_TITLE);

        // Defining Collapsing ToolBar values
        collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        collapsingToolbarLayout.setTitle(itemTitle);
        collapsingToolbarLayout.setExpandedTitleColor(getResources().getColor(
                android.R.color.transparent));
        collapsingToolbarLayout.setCollapsedTitleTextColor(0xffffffff);
        collapsingToolbarLayout.setExpandedTitleColor(0xffffffff);

        // Setting the title of the action bar for only when it's collapsed
        AppBarLayout appBarLayout = (AppBarLayout) findViewById(R.id.app_bar_layout);
        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            boolean isShown     = false;
            int     scrollRange = -1;
            // Set title only when the bar is collapsed
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if(scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                if(scrollRange + verticalOffset == 0) {
                    collapsingToolbarLayout.setTitle("SKY");
                    isShown = true;
                } else if(isShown) {
                    collapsingToolbarLayout.setTitle("");
                    isShown = false;
                }
            }
        });
        username = (MaterialEditText) findViewById(R.id.loginText);
        password = (MaterialEditText) findViewById(R.id.senhaText);
    }

    //Disable Android Back Button
    @Override
    public void onBackPressed() {
    // Do nothing
    }

    // API connection
    private void httpRequest(final String[] userData)
    {
        LoginRequest loginTask = new LoginRequest(new LoginRequest.LoginResponse()
        {

            @Override
            public void processFinish(Object output)
            {
                String[] loginRequestData = output.toString().split(" ");
                if(loginRequestData[0].equals("200")) {
                    Toast.makeText(getApplicationContext(), "Login...", Toast.LENGTH_SHORT).show();
                    MyApplication mApp = ((MyApplication)getApplicationContext());
                    mApp.setGlobalVarValue(loginRequestData[1] + " " + loginRequestData[2]);
                    //String globalVarValue = mApp.getGlobalVarValue();
                    Intent intent = new Intent(LoginActivity.this, MapsActivity.class);
                    intent.putExtra("username", loginRequestData[1] + " " + loginRequestData[2]);
                    startActivity(intent);
                } else if(output.equals("404")) {
                    Toast.makeText(getApplicationContext(), "Usuário ou senha incorretos",
                                   Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getApplicationContext(), "Servidor indisponível",
                                   Toast.LENGTH_SHORT).show();
                }
            }
        });
        loginTask.execute(userData);
    }
}

package br.com.pedrocunial.maptest.model;

import android.app.Application;

/**
 * Created by summerjob on 03/08/16.
 */
public class MyApplication extends Application {

    private String mGlobalVarValue;

    public String getGlobalVarValue() {
        return mGlobalVarValue;
    }

    public void setGlobalVarValue(String str) {
        mGlobalVarValue = str;
    }
}

package br.com.pedrocunial.maptest.model;

import android.content.Context;
import android.content.res.Resources;

import br.com.pedrocunial.maptest.R;
import br.com.pedrocunial.maptest.utils.InvalidOptionException;

/**
 * Created by summerjob on 26/07/16.
 */
public class OSInfo
{
    private String address;
    private String status;


    public OSInfo(Context context, String address)
    {
        this.address = address;
        this.status  = context.getResources().getString(R.string.pendente);
    }

    public OSInfo(Context context, String address, String status) throws InvalidOptionException
    {
        Resources cResources = context.getResources();
        if(status == cResources.getString(R.string.pendente) ||
           status == cResources.getString(R.string.resolvido)||
                status == cResources.getString(R.string.nao_resolvido)||
                status == cResources.getString(R.string.os_errada) ||
                status == cResources.getString(R.string.retorno)) {
            this.address = address;
            this.status  = status;
        } else {
            throw new InvalidOptionException();
        }
    }

    public String getAddress()
    {
        return address;
    }

    public String getStatus()
    {
        return status;
    }

    public void setStatus(String status)
    {
        this.status = status;
    }
}
